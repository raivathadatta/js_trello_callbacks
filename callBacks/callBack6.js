/* 
	Problem 6: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
let bord = require('../src/boards_2.json')
const getListBacedOnBordId = require("./callBack2.js");
const getCardDetailsBacedOnListId = require("./callBack3.js");
const getDetailsFromBord = require("./callback1.js");

function getAllStones(){
    try {
        let thanoesId = bord.filter((villen) => {
            return villen['name'].includes('Thanos')
        })[0]['id']
        console.log(thanoesId)
        getDetailsFromBord(thanoesId, (data) => {
            getListBacedOnBordId(data[0]['id'], ( data1) => {
                let ids = data1
                console.log(ids)
                ids.forEach(element => {
                    getCardDetailsBacedOnListId(element['id'], (mindCard) => {
                        console.log(mindCard)
                    })
                });
               
            })
        })
    } catch (err) {
        console.log(err)
    }
}
module.exports = getAllStones