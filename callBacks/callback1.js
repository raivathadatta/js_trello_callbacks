/* 
    Problem 1: Write a function that will return a particular board's 
    information based on the boardID that is passed from the given list 
    of boards in boards.board and then pass control back to the code that 
    called it by using a callback function.
*/
// board -- list -- card


let board = require('../src/boards_2.json')


function getDetailsFromBord(id, callback) {
    try {
        setTimeout(() => {
            let data1 = board.filter((elements) => elements['id'] == id)
            callback(data1) 
        }, 2000);
    } catch (err) {
        callback(new Error(`${err}`))
    }
}
module.exports = getDetailsFromBord
