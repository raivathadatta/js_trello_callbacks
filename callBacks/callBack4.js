/* 
    Problem 4: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const getListBacedOnBordId = require("./callBack2");
const getCardDetailsBacedOnListId = require("./callBack3");
const getDetailsFromBord = require("./callback1.js");

let bord = require('../src/boards_2.json')
function getAllThanoes() {
    try {
        let thanoesId = bord.filter((villen) => {
            return villen['name'].includes('Thanos')
        })[0]['id']
        console.log(thanoesId)
        getDetailsFromBord(thanoesId, (data) => {
            console.log(data)
            getListBacedOnBordId(data[0]['id'], (data1) => {
                console.log(data1)
                let ids = data1.filter((stone) => {
                    console.log(stone, "hh")
                    if (stone['name'] == 'Mind') {
                        return true
                    }
                    else {
                        return false
                    }
                })
                console.log(ids)
                ids.forEach(element => {
                    getCardDetailsBacedOnListId(element['id'], (mindCard) => {
                        console.log(mindCard)
                    })
                });
            })
        })
    } catch (err) {
        console.log(err)
    }
}
module.exports = getAllThanoes
// export default getAllThanoes
