/* 
    Problem 2: Write a function that will return all lists that belong to a board based
     on the boardID that is passed to it from the given data in lists.json. Then pass control back 
     to the code that called it by using a callback function.
*/

const list = require('../src/lists_1.json')
function getListBacedOnBordId(boardId, cb) {
    try {
        setTimeout(() => {
            console.log(boardId, list[boardId])
            cb(list[boardId])
        }, 2000);
    } catch (err) {
        console.log(err)
    }
}

module.exports = getListBacedOnBordId
